# Social Flexible Group Types

In Open Social there is a special group type called "flexible_group".
If you would need an extra group type with the same out of the box functionality this module is for you.

This module provides handling for custom flexible groups.
To make it work you must have the following existing fields attached to your manually created group type:
- field_group_allowed_visibility
- field_flexible_group_visibility
- field_group_allowed_join_method

It's recommended to duplicate the open social field group concept for groups, to optimize the open social integration. 
Have a look over here: admin/group/types/manage/flexible_group/form-display

## Installation

Install like any other module.

## Dependencies

You have to have open social installed and the social_group module enabled.


## Configuration

Your custom flexible groups will be detected automatically at admin/config/opensocial/social-flexible-group-types.
Please tick them to activate the desired group logic.

To complete the integration you have to create a custom module holding all the machine names of the manually created group types.
Please insert the following HOOK into the .module file.

```
function YOURMODULE_social_group_types_alter(array &$social_group_types) {

  // Define your group types here
  $social_group_types[] = 'GROUP_TYPE'

}

```

Please keep in mind that you have to manually set the group permissions, configure the all group content items, extend the contextual filter and filter inside the available views (Example VIEW: group_events).


